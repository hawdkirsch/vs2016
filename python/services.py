#!flask/bin/python
from flask import Flask, json, request
import cv2
import numpy as np
import base64

app = Flask(__name__)
 
@app.route('/')
def index():
	return "Try out ../get_image or ../get_position."

@app.route('/get_image', methods=['GET'])
def get_image():
	camera = cv2.VideoCapture(0)
	
	for i in xrange(30):
		_, temp = camera.read()
	
	_, camera_capture = camera.read()
	(image_height, image_width, third) = camera_capture.shape
	ret_string = base64.b64encode(camera_capture.tostring())
	
	print("b64len: "+str(len(ret_string))+" camera_capture: "+str(len(camera_capture.tostring())))
	
	del(camera)
	return '[{"b64": "'+ret_string+'", "height": "'+str(image_height)+'", "width": "'+str(image_width)+'", "third": "'+str(third)+'"}]'
	
	
def getRGB(RGBint):
    blue =  RGBint & 255
    green = (RGBint >> 8) & 255
    red =   (RGBint >> 16) & 255
    return red, green, blue
    
def getAvgFromMask(mask, width, height):
	x_counter = 0
	y_counter = 0
	x_div = 0
	y_div = 0

	for (x,y), value in np.ndenumerate(mask):
		if value==255:
			x_counter = x_counter+x
			y_counter = y_counter+y
			
			x_div = x_div+1
			y_div = y_div+1
	
	
	
	if x_div!=0:
		x_avg = x_counter/x_div
		y_avg = y_counter/y_div
			
		print("x_avg: "+str(x_avg)+ " width: "+str(width))
		print("y_avg: "+str(y_avg)+ " height: "+str(height))
		return 0, (x_avg*100)/width, (y_avg*100)/height
	else:
		return -1, -1, -1
	
@app.route('/get_position', methods=['POST'])
def get_position():
	ret = [{'status_left': -1, 'status_right': -1}]
	if not request.json or not 'left' in request.json or not 'right' in request.json or not 'image':
		return json.dumps(ret)
	
	jsonObj = json.loads(request.json['image'])
	
	b64image = jsonObj["b64"]
	height = int(jsonObj["height"])
	width = int(jsonObj["width"])
	third = int(jsonObj["third"])
	
	
	encoded_image = base64.decodestring(b64image)
	
	#decode base64 string and save as numpy array
	np_image = np.fromstring(encoded_image, dtype=np.uint8)
	
	image = np_image.reshape(height, width, third)	
	
	#offset for the cv2.inRange method (in %)
	offset = 0.3
		
	#get left and right rgb values and generate the lower and upper limit for cv2.inRange
	r_left, g_left, b_left = getRGB(request.json['left'])
	lower_left = np.array([b_left*offset, g_left*offset, r_left*offset], dtype = np.uint8)
	upper_left = np.array([b_left+(offset*(255-b_left)), g_left+(offset*(255-g_left)), r_left+(offset*(255-r_left))], dtype = np.uint8)
	
	r_right, g_right, b_right = getRGB(request.json['right'])
	lower_right = np.array([b_right*offset, g_right*offset, r_right*offset], dtype = "uint8")
	upper_right = np.array([b_right+(offset*(255-b_right)), g_right+(offset*(255-g_right)), r_right+(offset*(255-r_right))], dtype = np.uint8)
 
	#generate the masks which will contain 255 if the pixel is in range of
	#lower and upper or 0 if not
	mask_left = cv2.inRange(image, lower_left, upper_left)
	mask_right = cv2.inRange(image, lower_right, upper_right)
	
	cv2.imwrite("left.png", mask_left)

	#calculate the average pixel position with 255 in image
	ret[0]['status_left'], ret[0]['left_x'], ret[0]['left_y'] = getAvgFromMask(mask_left, width, height)
	ret[0]['status_right'], ret[0]['right_x'], ret[0]['right_y'] = getAvgFromMask(mask_right, width, height)
	
	return json.dumps(ret)
	
	
if __name__ == '__main__':
	import netifaces as ni
	ip = ni.ifaddresses('enp2s0f0')[2][0]['addr']
	app.run(host=ip, port=8000, debug=True)
	#app.run(debug=True)

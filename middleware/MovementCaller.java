package vs.middleware;

import java.util.LinkedList;
import java.util.concurrent.locks.Lock;

import org.cads.ev3.middeware.CaDSEV3StudentImplementation;

import vs.middleware.MovementManager.Movement;

public class MovementCaller implements Runnable{
	private LinkedList<Movement> fifo = null;
	private Lock mutex;
	private CaDSEV3StudentImplementation caller;
	
	MovementCaller(CaDSEV3StudentImplementation caller, LinkedList<Movement> fifo, Lock mutex){
		this.caller = caller;
		this.fifo = fifo;
		this.mutex = mutex;
	}
	
	/**
	 * Dequeue the Movements from the given queue and execute the corresponding action.
	 * While this thread calls moveLeft for example, it will block until the motor blocks.
	 */
	@Override
	public void run() {
		while(!Thread.currentThread().isInterrupted()){
			if (!fifo.isEmpty()){
				mutex.lock();
				Movement m = fifo.removeFirst();
				mutex.unlock();
				
				switch(m){
					case LEFT : {
						caller.moveLeft(); 
						break;
					}
					case RIGHT : {
						caller.moveRight();
						break;
					}
					case UP : {
						caller.moveUp();
						break;
					}
					case DOWN : {
						caller.moveDown();
						break;
					}
				}
			}
		}
	}
}

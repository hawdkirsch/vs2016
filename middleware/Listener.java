package vs.middleware;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.cads.ev3.middeware.ICaDSEV3FeedBackListener;
import org.cads.ev3.middeware.ICaDSEV3StatusListener;
import org.json.simple.JSONObject;

public class Listener implements ICaDSEV3StatusListener, ICaDSEV3FeedBackListener {
	private static Listener instance;
	
	private static MovementManager mManager;
	
	private int[] clientPosition = new int[3];
	private boolean[] isMoving = new boolean[4];
	private boolean isGrabbing = false;
	private Properties prop;
	private InputStream in;
	
	private final int HORIZONTAL;
	private final int VERTICAL;
	private final int GRABBER;
	
	private final int LEFT;//0
	private final int RIGHT;//1
	private final int UP;//2
	private final int DOWN;//3
	
	private final Logger listenerLog = Logger.getLogger(Consumer.class.getName());
	
	
	private Listener(){
		for(int i=0; i<3; i++){
			clientPosition[i]=0;			
		}
		
		for(int i=0; i<4; i++){
			isMoving[i]=false;			
		} 
		
		mManager = MovementManager.getInstance(this, this);
		prop = new Properties();
		in = getClass().getResourceAsStream("config.properties");
		try {
			prop.load(in);
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		 HORIZONTAL = Integer.parseInt((prop.getProperty("HORIZONTAL")));
		 VERTICAL = Integer.parseInt((prop.getProperty("VERTICAL")));
		 GRABBER = Integer.parseInt((prop.getProperty("GRABBER")));
		 LEFT = Integer.parseInt((prop.getProperty("LEFT")));
		 RIGHT = Integer.parseInt((prop.getProperty("RIGHT")));
		 UP = Integer.parseInt((prop.getProperty("UP")));
		 DOWN = Integer.parseInt((prop.getProperty("DOWN")));
		
	};
	
	
	/**
	 * Singleton Pattern
	 * This class is allowed to only be instanced once. If it doesn't exist yet it will be created.
	 * @return The instance.
	 */	
	public static Listener getInstance(){
		if (instance==null)
			instance = new Listener();
		
		return instance;
	}
	
	
	/**
	 * Refresh the wanted position of the crane from the client.
	 * @param type One of the three types: Listener.HORIZONTAL, Listener.VERTICAL or Listener.GRAB
	 * @param value Percent value (between 0 and 100) of the wanted position of the given "type"
	 * @return true if the parameters fulfill the requirements, false if they don't
	 */
	public boolean setPosition(int type, int value){
		if (type != HORIZONTAL && type != VERTICAL && type != GRABBER)
			return false;
		else if (value < 0 || value > 100)
			return false;
		
		clientPosition[type]=value;
		return true;
	}

	
	/**
	 * Not interesting for the current assignment (that's what I hope at least)s
	 */
	@Override
	public synchronized void giveFeedbackByJSonTo(JSONObject arg0) {
		//Not interesting at the moment
	}	

	
	/**
	 * Logic of the server-side middleware.
	 * Use the given brick-interface to set the crane's position as wanted from the client. Once this function terminates the thread,
	 * or in this case the whole server, dies, so it should better be kept alive!
	 */
	@Override
	public synchronized void onStatusMessage(JSONObject arg0) {
		if (arg0==null || !arg0.containsKey("percent") || !arg0.containsKey("state")){
			return;
		}
		
			
		long value = (long) arg0.get("percent");
		String state = (String) arg0.get("state");
		
		/**
		 * Dispatcher Pattern
		 * Manage the events (status messages) from the brick/simulation and
		 * Dispatch the event to the MovementManager.
		 */
		switch(state){
			case "horizontal" : {
				if (clientPosition[HORIZONTAL]+2>=value && clientPosition[HORIZONTAL]-2<=value){		//arrived at horizontal position
					if (isMoving[LEFT] || isMoving[RIGHT]){		//stop if he is moving horizontally
						mManager.stopH();
						isMoving[LEFT] = false;
						isMoving[RIGHT] = false;
						listenerLog.log(Level.FINE, "[INFO] stop horizontal movement");
					}
				}
				else if (clientPosition[HORIZONTAL]>value){	//has to move LEFT
					if (!isMoving[LEFT]){						//if he isn't moving LEFT already
						mManager.stopH();
						mManager.moveLeft();
						isMoving[LEFT] = true;
						isMoving[RIGHT] = false;
						listenerLog.log(Level.FINE, "[INFO] move left");
					}
				}
				else if (clientPosition[HORIZONTAL]<value){	//has to move RIGHT
					if (!isMoving[RIGHT]){						//if he isn't moving RIGHT already
						mManager.stopH();
						mManager.moveRight();
						isMoving[RIGHT] = true;
						isMoving[LEFT] = false;
						listenerLog.log(Level.FINE, "[INFO] move right");
					}
				}
				break;
			}
			
			case "vertical" : {
				if (clientPosition[VERTICAL]+2>=value && clientPosition[VERTICAL]-2<=value){		//arrived at vertical position
					if (isMoving[UP] || isMoving[DOWN]){		//stop if he is moving vertically
						mManager.stopV();
						isMoving[UP] = false;
						isMoving[DOWN] = false;
						listenerLog.log(Level.FINE, "[INFO] stop vertical movement");
					}
				}
				else if (clientPosition[VERTICAL]>value){	//has to move UP
					if (!isMoving[UP]){							//if he isn't moving UP already
						mManager.stopV();
						mManager.moveUp();
						isMoving[UP] = true;
						isMoving[DOWN] = false;
						listenerLog.log(Level.FINE, "[INFO] move up");
					}
				}
				else if (clientPosition[VERTICAL]<value){	//has to move DOWN
					if (!isMoving[DOWN]){						//if he isn't moving DOWN already
						mManager.stopV();
						mManager.moveDown();
						isMoving[DOWN] = true;
						isMoving[UP] = false;
						listenerLog.log(Level.FINE, "[INFO] move down");
					}
				}
				break;
			}
			
			case "grap" : {
				if (clientPosition[GRABBER]==100 && !isGrabbing){		//has to GRAB
					mManager.doGrab();
					isGrabbing = true;
					listenerLog.log(Level.FINE, "[INFO] do grab");
				}
				else if (clientPosition[GRABBER]==0 && isGrabbing){	//has to RELEASE
					mManager.doRelease();
					isGrabbing = false;
					listenerLog.log(Level.FINE, "[INFO] do release");
				}
				break;
			}
		}	
	}
}

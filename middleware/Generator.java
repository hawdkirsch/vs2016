package vs.middleware;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Generator {
	public static String pkg = "vs.middleware.generated";
	public static String directory = "/home/students/abp842/TI_Labor/Linux/eclipse_mars/VSMiddleware/src/vs/middleware/generated/";
	
	public static void main(String[] args) {
		generateStubs();
		generateInterfaces();
		generateSkeletons();
	}
		
	public static void generateSkeletons(){		
		File idlDir = new File(directory+"idl/");
		File[] idlDirectoryListing = idlDir.listFiles();
		if (idlDirectoryListing == null) {
			System.out.println("directory not found");
			return;
		}

		System.out.println("\nGenerating skeletons...");
		String skeletonOut = "package "+pkg+";\nimport org.json.simple.JSONObject;\nimport vs.middleware.CommunicationServer;\nimport vs.middleware.Provider;\n\n"
				+"public class Skeleton implements Runnable{\n";
		String objectInitialization = "";
		String constructor = "\tpublic Skeleton(){\n"
								+"\t\tProvider provider = new Provider();\n";
		String run = "\t@Override\n\tpublic void run(){\n\t\t"
						+"while(!Thread.currentThread().isInterrupted()){\n\t\t\t"
						+"while(CommunicationServer.getInstance(\"\",0,0).messageQueue.isEmpty()){}\n\t\t\t"
						+"JSONObject jsonIn = CommunicationServer.getInstance(\"\",0,0).messageQueue.poll();\n\t\t\t"
						+"switch((String) jsonIn.get(\"object\")){\n";
		
		int objectCounter = 1;
		for (File idlFile : idlDirectoryListing) {
			JSONObject idlJson;
			try{
				Scanner scan = new Scanner(idlFile); 
				String idlFileContent = scan.useDelimiter("\\Z").next();
				scan.close();
				idlJson = (JSONObject) (new JSONParser().parse(idlFileContent));
			} catch(FileNotFoundException e){
				System.out.println("file not found");
				return;
			} catch(ParseException e){
				System.out.println("error in parsin json object");
				return;
			}
			
			if (!idlJson.containsKey("class_name")){
				System.out.println("wrong idl file format (class name not found)");
				return;
			}
			
			/**
			 * Read IDL file and extract parameters
			 */
			String className = (String) idlJson.get("class_name");
			
			
			Map<String, List<List<String>>> functionMap = new HashMap<>();
			int counter = 1;
			while(idlJson.containsKey("function_"+counter)){
				final List<List<String>> paramList = new ArrayList<>();
				String functionName = (String) idlJson.get("function_"+counter);
				JSONArray paramArray = (JSONArray) idlJson.get("function_"+counter+"_param");
				
				counter++;
				if (paramArray==null){
					functionMap.put(functionName, null);
					continue;
				}
				
				List<String> parameter;				
				for (int i=0; i<paramArray.size(); i++){
					parameter = new ArrayList<>();
					JSONObject paramJson = (JSONObject) paramArray.get(i);
					parameter.add((String) paramJson.get("type"));
					parameter.add((String) paramJson.get("name"));
					
					paramList.add(parameter);
				}
								
				functionMap.put(functionName, paramList);
			}
			/**
			 * End
			 */
			
			
			/**
			 * Write java class and save as file			
			 */		
			run += "\t\t\t\tcase \""+className+"\":{\n"
					+"\t\t\t\t\tswitch((String) jsonIn.get(\"method\")){\n";
			
			for (String functionName : functionMap.keySet()){
				objectInitialization += "\tI"+className+" obj"+objectCounter+";\n";
				constructor += "\t\tobj"+objectCounter+" = provider;\n";
				
				run += "\t\t\t\t\t\tcase \""+functionName+"\":{\n";
				
				
				String functionCall = "\t\t\t\t\t\t\tobj"+objectCounter+"."+functionName+"(";
				List<List<String>> paramList = functionMap.get(functionName);
				if (paramList != null){
					for (int j=0; j<paramList.size(); j++){
						functionCall += "(int) ((long) jsonIn.get(\""+paramList.get(j).get(1)+"\"))";
						
						if (j!=paramList.size()-1){
							functionCall += ", ";
						}
					}
				}
				
				run += functionCall+");\n\t\t\t\t\t\t}\n";
			}
			
			run += "\t\t\t\t\t}\n"
					+"\t\t\t\t}\n";
			
			objectCounter++;
		}	
		
		run += "\t\t\t}\n"
				+"\t\t}\n"
				+"\t}\n";
		
		skeletonOut += objectInitialization+"\n"+constructor+"\t}\n\n"+run+"}\n";
		
		PrintWriter fileOut = null;
		try {
			fileOut = new PrintWriter(directory+"Skeleton.java");
		} catch (FileNotFoundException e) {
			System.out.println("output file could not be opened");
			return;
		}
		fileOut.println(skeletonOut);
		System.out.println("Skeleton file generated");
		fileOut.close();
	}
	
	public static void generateInterfaces(){
		File idlDir = new File(directory+"idl/");
		File[] idlDirectoryListing = idlDir.listFiles();
		if (idlDirectoryListing == null) {
			System.out.println("directory not found");
			return;
		}
		
		System.out.println("\nGenerating interfaces...");
		for (File idlFile : idlDirectoryListing) {
			JSONObject idlJson;
			try{
				Scanner scan = new Scanner(idlFile); 
				String idlFileContent = scan.useDelimiter("\\Z").next();
				scan.close();
				idlJson = (JSONObject) (new JSONParser().parse(idlFileContent));
			} catch(FileNotFoundException e){
				System.out.println("file not found");
				return;
			} catch(ParseException e){
				System.out.println("error in parsin json object");
				return;
			}
			
			if (!idlJson.containsKey("class_name")){
				System.out.println("wrong idl file format (class name not found)");
				return;
			}
			
			/**
			 * Read IDL file and extract parameters
			 */
			String className = (String) idlJson.get("class_name");
			
			
			Map<String, List<List<String>>> functionMap = new HashMap<>();
			int counter = 1;
			while(idlJson.containsKey("function_"+counter)){
				final List<List<String>> paramList = new ArrayList<>();
				String functionName = (String) idlJson.get("function_"+counter);
				String returnType = (String) idlJson.get("return_"+counter);
				JSONArray paramArray = (JSONArray) idlJson.get("function_"+counter+"_param");
				
				counter++;
				
				if (returnType != null){
					functionName += "!"+returnType;
				}
				
				if (paramArray==null){
					functionMap.put(functionName, null);
					continue;
				}
				
				List<String> parameter;				
				for (int i=0; i<paramArray.size(); i++){
					parameter = new ArrayList<>();
					JSONObject paramJson = (JSONObject) paramArray.get(i);
					parameter.add((String) paramJson.get("type"));
					parameter.add((String) paramJson.get("name"));
					
					paramList.add(parameter);
				}
								
				functionMap.put(functionName, paramList);
			}
			/**
			 * End
			 */
			
			
			/**
			 * Write in stub template and save as file			
			 */			
			String interfaceOut = "package "+pkg+";\nimport org.json.simple.JSONObject;\n\n"
								+"public interface I"+className+"{\n";
			
			
			String functionOut = null;
			
			for (String functionName : functionMap.keySet()){
				functionOut = "\tpublic ";
				
				if (functionName.contains("!")){
					functionOut += functionName.replaceAll("^.+!", "")+" "+functionName.replaceAll("!.+$", "")+"(";
				} else {
					functionOut += "void "+functionName+"(";
				}
				
				List<List<String>> paramList = functionMap.get(functionName);
				if (paramList != null){
					for (int j=0; j<paramList.size(); j++){
						functionOut += paramList.get(j).get(0)+" "+paramList.get(j).get(1);
						
						if (j!=paramList.size()-1){
							functionOut += ", ";
						}
					}
				}
				
				interfaceOut += functionOut+");\n}\n";
			}
			
			
			PrintWriter fileOut = null;
			try {
				fileOut = new PrintWriter(directory+"I"+className+".java");
			} catch (FileNotFoundException e) {
				System.out.println("output file could not be opened");
				return;
			}
			fileOut.println(interfaceOut);
			System.out.println("I"+className+" interface file generated");
			fileOut.close();
		}
	}
	
	public static void generateStubs(){
		File idlDir = new File(directory+"idl/");
		File[] idlDirectoryListing = idlDir.listFiles();
		if (idlDirectoryListing == null) {
			System.out.println("directory not found");
			return;
		}

		System.out.println("\nGenerating stubs...");
		for (File idlFile : idlDirectoryListing) {
			JSONObject idlJson;
			try{
				Scanner scan = new Scanner(idlFile); 
				String idlFileContent = scan.useDelimiter("\\Z").next();
				scan.close();
				idlJson = (JSONObject) (new JSONParser().parse(idlFileContent));
			} catch(FileNotFoundException e){
				System.out.println("file not found");
				return;
			} catch(ParseException e){
				System.out.println("error in parsin json object");
				return;
			}
			
			if (!idlJson.containsKey("class_name")){
				System.out.println("wrong idl file format (class name not found)");
				return;
			}
			
			/**
			 * Read IDL file and extract parameters
			 */
			String className = (String) idlJson.get("class_name");
			
			
			Map<String, List<List<String>>> functionMap = new HashMap<>();
			int counter = 1;
			while(idlJson.containsKey("function_"+counter)){
				final List<List<String>> paramList = new ArrayList<>();
				String functionName = (String) idlJson.get("function_"+counter);
				String returnType = (String) idlJson.get("return_"+counter);
				JSONArray paramArray = (JSONArray) idlJson.get("function_"+counter+"_param");
				
				counter++;
				
				if (returnType != null){
					functionName += "!"+returnType;
				}
				
				if (paramArray==null){
					functionMap.put(functionName, null);
					continue;
				}
				
				List<String> parameter;				
				for (int i=0; i<paramArray.size(); i++){
					parameter = new ArrayList<>();
					JSONObject paramJson = (JSONObject) paramArray.get(i);
					parameter.add((String) paramJson.get("type"));
					parameter.add((String) paramJson.get("name"));
					
					paramList.add(parameter);
				}
							
				functionMap.put(functionName, paramList);
			}
			/**
			 * End
			 */
			
			
			/**
			 * Write java class and save as file			
			 */			
			String stubOut = "package "+pkg+";\nimport org.json.simple.JSONObject;\nimport vs.middleware.CommunicationClient;\n\n"
								+"public class "+className+"Impl implements I"+className+"{\n";
			
			
			String functionOut = null;
			
			for (String functionName : functionMap.keySet()){
				functionOut = "\t@Override\n\tpublic ";
				
				if (functionName.contains("!")){
					functionOut += functionName.replaceAll("^.+!", "")+" "+functionName.replaceAll("!.+$", "")+"(";
				} else {
					functionOut += "void "+functionName+"(";
				}
				
				String putString = "jsonOut.put(\"object\", \""+className+"\");\n\t\t"
						+"jsonOut.put(\"method\", \""+functionName.replaceAll("!.+$", "")+"\");\n\t\t";
				List<List<String>> paramList = functionMap.get(functionName);
				if (paramList != null){
					for (int j=0; j<paramList.size(); j++){
						functionOut += paramList.get(j).get(0)+" "+paramList.get(j).get(1);
						putString += "jsonOut.put(\""+paramList.get(j).get(1)+"\", "+paramList.get(j).get(1)+");\n\t\t";
						
						if (j!=paramList.size()-1){
							functionOut += ", ";
						}
					}
				}
				
				functionOut += "){\n\t\t"
								+"JSONObject jsonOut = new JSONObject();\n\t\t"
								+"CommunicationClient com = CommunicationClient.getInstance();\n\n\t\t"
								+putString
								+"com.sendRemoteMethodCall(jsonOut);\n\t}\n}\n";
				
				stubOut += functionOut;
			}
			
			
			PrintWriter fileOut = null;
			try {
				fileOut = new PrintWriter(directory+className+"Impl.java");
			} catch (FileNotFoundException e) {
				System.out.println("output file could not be opened");
				return;
			}
			fileOut.println(stubOut);
			System.out.println(className+"Impl stub file generated");
			fileOut.close();
		}		
	}
}

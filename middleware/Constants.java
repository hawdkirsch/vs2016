package vs.middleware;

public class Constants {
	public static final int portH = 50042;
	public static final int portV = 50043;
	public static final int portG = 50044;
	
	public static final int CLOSE = 100;
	public static final int OPEN = 0;
	
	public static final int HORIZONTAL = 0;
	public static final int VERTICAL = 1;
	public static final int GRABBER = 2;
	
	public static final int COORDINATOR_PUBLISH_PORT = 50052;
	public static final int COORDINATOR_LOOKUP_PORT = 50053;
	public static final int COORDINATOR_CONCEAL_PORT = 50054;
}

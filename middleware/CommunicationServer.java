package vs.middleware;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Properties;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class CommunicationServer implements Runnable {
	private Logger logger;
	private Properties properties;
	private String proxyAddress;
	private int proxyPort;
	private Socket proxySocket;
	private BufferedReader proxyInput;
	private DataOutputStream proxyOutput;
	private int which;
	public ConcurrentLinkedQueue<JSONObject> messageQueue;
	private static CommunicationServer instance;
	
	private CommunicationServer(String address, int port, int which){
		logger = Logger.getLogger(Consumer.class.getName());
		try {
			FileHandler fh = new FileHandler("communication_server.log");
			logger.addHandler(fh);
			fh.setFormatter(new SimpleFormatter());
			logger.info("startet without problem");
			logger.setLevel(Level.OFF);
		} catch (Exception e) {
			logger.severe("log file could not be opened");
			return;
		}
		
		properties = new Properties();
		InputStream in = getClass().getResourceAsStream("config.properties");
		try {
			properties.load(in);
			in.close();
		} catch(IOException e) {
			logger.severe("failed to read properties");
			return;
		}	
		
		this.which = which;
		messageQueue = new ConcurrentLinkedQueue<>();
		proxyAddress = address;
		proxyPort = port;
		
		establishConnectionToProxy();
	}
	
	public static CommunicationServer getInstance(String address, int port, int which){
		if (instance==null){
			instance = new CommunicationServer(address, port, which);
		}
		
		return instance;
	}
	
	public void sendPublish(){		
		JSONObject publish = new JSONObject();
		publish.put("robot_id", which);
		publish.put("proxy_ip", proxyAddress);

		try {
			while(proxyOutput==null){}
			proxyOutput.writeBytes(publish.toString() + "\n");
			logger.info("sent publish message to proxy "+publish.toString());
		} catch (IOException e) {
			logger.warning("publish message couldn't be send to proxy"+which+", "+publish.toString());
		}
	}
	
	private void establishConnectionToProxy(){
		try{
			proxySocket = new Socket(proxyAddress, proxyPort);
			logger.info("socket for proxy"+which+" created, address "+proxyAddress+", port "+proxyPort);
		} catch(IOException e){
			logger.severe("failed to create socket for proxy"+which+", address "+proxyAddress+", port "+proxyPort);
			return;
		}
		
		try{
			proxyInput = new BufferedReader(new InputStreamReader(proxySocket.getInputStream()));
			logger.info("opened input stream for proxy"+which);
		} catch(IOException ioe){
			try{
				proxySocket.close();
			} catch(Exception e){}
			logger.severe("failed to open input stream for proxy"+which);
			return;
		}
		
		try{
			proxyOutput = new DataOutputStream(proxySocket.getOutputStream());
			logger.info("opened output stream for proxy"+which);
		} catch(IOException ioe){
			try{
				proxyInput.close();
				proxySocket.close();
			} catch(Exception e){}
			logger.severe("failed to open output stream for proxy"+which);
			return;
		}
		
		return;		
	}	
	
	private void processProxyMessage(JSONObject jsonIn) {
		if (jsonIn == null || !jsonIn.containsKey("robot_id") || !jsonIn.containsKey("degree") 
				|| !jsonIn.containsKey("object") || !jsonIn.containsKey("method")){
			logger.severe("wrong format from remote method call message "+jsonIn.toString());
			return;
		}

		logger.severe("received remote method call message "+jsonIn.toString());
		messageQueue.add(jsonIn);
	}
	
	public void run(){		
		try {
			String textIn;
			
			while ((textIn = proxyInput.readLine()) != null){
				JSONObject jsonIn = null;
				try {
					jsonIn = (JSONObject) (new JSONParser().parse(textIn));
				} catch (ParseException e) {
					logger.info("failed to parse received proxy message "+textIn);
					continue;
				}

				logger.info("received proxy message "+jsonIn.toString());
				processProxyMessage(jsonIn);
			}
		} catch (IOException ioe){
			try{
				proxyOutput.close();
				proxySocket.close();
				proxyInput.close();
			} catch(Exception e){}
			logger.info("failed to read proxy message, close connection");
		}
	}
}

package vs.middleware.generated;
import org.json.simple.JSONObject;

public interface IVerticalMotor{
	public void moveVerticalToDegree(int robot_id, int degree);
}


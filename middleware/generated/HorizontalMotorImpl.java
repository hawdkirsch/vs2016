package vs.middleware.generated;
import org.json.simple.JSONObject;
import vs.middleware.CommunicationClient;

public class HorizontalMotorImpl implements IHorizontalMotor{
	@Override
	public void moveHorizontalToDegree(int robot_id, int degree){
		JSONObject jsonOut = new JSONObject();
		CommunicationClient com = CommunicationClient.getInstance();

		jsonOut.put("object", "HorizontalMotor");
		jsonOut.put("method", "moveHorizontalToDegree");
		jsonOut.put("robot_id", robot_id);
		jsonOut.put("degree", degree);
		com.sendRemoteMethodCall(jsonOut);
	}
}


package vs.middleware.generated;
import org.json.simple.JSONObject;

public interface IHorizontalMotor{
	public void moveHorizontalToDegree(int robot_id, int degree);
}


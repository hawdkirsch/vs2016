package vs.middleware.generated;
import org.json.simple.JSONObject;
import vs.middleware.CommunicationServer;
import vs.middleware.Provider;

public class Skeleton implements Runnable{
	IVerticalMotor obj1;
	IGrabberMotor obj2;
	IHorizontalMotor obj3;

	public Skeleton(){
		Provider provider = new Provider();
		obj1 = provider;
		obj2 = provider;
		obj3 = provider;
	}

	@Override
	public void run(){
		while(!Thread.currentThread().isInterrupted()){
			while(CommunicationServer.getInstance("",0,0).messageQueue.isEmpty()){}
			JSONObject jsonIn = CommunicationServer.getInstance("",0,0).messageQueue.poll();
			switch((String) jsonIn.get("object")){
				case "VerticalMotor":{
					switch((String) jsonIn.get("method")){
						case "moveVerticalToDegree":{
							obj1.moveVerticalToDegree((int) ((long) jsonIn.get("robot_id")), (int) ((long) jsonIn.get("degree")));
						}
					}
				}
				case "GrabberMotor":{
					switch((String) jsonIn.get("method")){
						case "doGrab":{
							obj2.doGrab((int) ((long) jsonIn.get("robot_id")), (int) ((long) jsonIn.get("degree")));
						}
					}
				}
				case "HorizontalMotor":{
					switch((String) jsonIn.get("method")){
						case "moveHorizontalToDegree":{
							obj3.moveHorizontalToDegree((int) ((long) jsonIn.get("robot_id")), (int) ((long) jsonIn.get("degree")));
						}
					}
				}
			}
		}
	}
}


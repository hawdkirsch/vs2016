package vs.middleware.generated;
import org.json.simple.JSONObject;
import vs.middleware.CommunicationClient;

public class VerticalMotorImpl implements IVerticalMotor{
	@Override
	public void moveVerticalToDegree(int robot_id, int degree){
		JSONObject jsonOut = new JSONObject();
		CommunicationClient com = CommunicationClient.getInstance();

		jsonOut.put("object", "VerticalMotor");
		jsonOut.put("method", "moveVerticalToDegree");
		jsonOut.put("robot_id", robot_id);
		jsonOut.put("degree", degree);
		com.sendRemoteMethodCall(jsonOut);
	}
}


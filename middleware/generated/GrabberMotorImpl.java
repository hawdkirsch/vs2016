package vs.middleware.generated;
import org.json.simple.JSONObject;
import vs.middleware.CommunicationClient;

public class GrabberMotorImpl implements IGrabberMotor{
	@Override
	public void doGrab(int robot_id, int degree){
		JSONObject jsonOut = new JSONObject();
		CommunicationClient com = CommunicationClient.getInstance();

		jsonOut.put("object", "GrabberMotor");
		jsonOut.put("method", "doGrab");
		jsonOut.put("robot_id", robot_id);
		jsonOut.put("degree", degree);
		com.sendRemoteMethodCall(jsonOut);
	}
}


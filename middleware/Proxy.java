package vs.middleware;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;
import java.util.Queue;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class Proxy implements Runnable{
	private Logger logger;
	private Properties properties;
	private int providerPort;
	private int clientPort;
	public Queue<JSONObject> messageQueue;
	ProviderPublishListener publishListener;
	
	public static void main(String[] args) {
		int providerPort = 0;
		int clientPort = 0;
		int count = 0;
		
		for(String s : args){
			switch(s){
				case "-pp":{
					providerPort = Integer.parseInt(args[count+1]);
					break;
				}
				case "-cp":{
					clientPort = Integer.parseInt(args[count+1]);
					break;
				}
			}
			count++;
		}
		
		(new Thread(new Proxy(providerPort, clientPort))).start();
	}
	
	public Proxy(int providerPort, int clientPort){
		logger = Logger.getLogger(Consumer.class.getName());
		try {
			FileHandler fh = new FileHandler("proxy.log");
			logger.addHandler(fh);
			fh.setFormatter(new SimpleFormatter());
			logger.info("startet without problem");
		} catch (Exception e) {
			logger.severe("log file could not be opened");
			return;
		}
		
		properties = new Properties();
		InputStream in = getClass().getResourceAsStream("config.properties");
		try {
			properties.load(in);
			in.close();
		} catch(IOException e) {
			logger.severe("failed to load properties");
			return;
		}	
		
		publishListener = new ProviderPublishListener();
		new Thread(publishListener).start();
		this.providerPort = providerPort;
		this.clientPort = clientPort;
	}
	
	public void run(){
		ServerSocket listenSocket = null;
		Socket clientSocket = null;

		try {
			listenSocket = new ServerSocket(clientPort);
			logger.info("listening on port "+clientPort);
		} catch (Exception e) {
			logger.severe("failed to listen on port "+clientPort);
			return;
		}
		
		while(!Thread.currentThread().isInterrupted()){
			try{
				clientSocket = listenSocket.accept();
				new Thread(new ProxyClientListener(clientSocket, publishListener)).start();
				logger.info("accepted new client");
			}catch(Exception e){}
		}
		
		try {
			listenSocket.close();
			logger.severe("listening socket closed");
		} catch (IOException e) {}
	}
	
	private class ProxyClientListener implements Runnable{
		Socket clientSocket;
		ProviderPublishListener provider;
		
		ProxyClientListener(Socket clientSocket, ProviderPublishListener provider){
			this.clientSocket = clientSocket;
			this.provider = provider;
		}
		
		@Override
		public void run() {
			//Open input stream for reader
			BufferedReader clientInput;
			try{
				clientInput = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
				logger.info("input stream for consumer opened");
			} catch(IOException ioe){
				try{
					clientSocket.close();
				} catch(Exception e){}
				logger.severe("failed to open input stream for consumer");
				return;
			}
			
			try{
				String forwardMessage;
				while((forwardMessage = clientInput.readLine()) != null){
					provider.providerOutput.writeBytes(forwardMessage +"\n");
					logger.info("forwarded message "+forwardMessage+" to provider");
				}
			}catch(Exception e){
				try {
					clientInput.close();
					clientSocket.close();
				} catch (IOException ioe) {}
				logger.severe("failed to forwarded message to provider");
			}
		}
	}
	
	enum MessageType{
		CONCEAL,
		PUBLISH
	}
	
	private class ProviderPublishListener implements Runnable{
		DataOutputStream providerOutput;
		
		private void sendToCoordinator(String toSend, MessageType type){		
			Socket coordinatorSocket;
			BufferedReader coordinatorInput;
			DataOutputStream coordinatorOutput;
			String address = properties.getProperty("COORDINATOR_ADDRESS");
			int port = 0;
			switch (type){
				case PUBLISH: {
					port = Integer.parseInt(properties.getProperty("COORDINATOR_PUBLISH_PORT"));
					break;
				}
				case CONCEAL: {
					port = Integer.parseInt(properties.getProperty("COORDINATOR_CONCEAL_PORT"));
					break;
				}
			}
			
			try{
				coordinatorSocket = new Socket(address, port);
				logger.info("opened coordinator socket on address "+address+", port "+port);
			} catch(IOException e){
				logger.severe("failed to open coordinator socket on port "+port);
				return;
			}
			
			try{
				coordinatorInput = new BufferedReader(new InputStreamReader(coordinatorSocket.getInputStream()));
				logger.info("opened input stream for coordinator");
			} catch(IOException ioe){
				try{
					coordinatorSocket.close();
				} catch(Exception e){}
				logger.severe("failed to open input stream for coordinator");
				return;
			}

			try{
				coordinatorOutput = new DataOutputStream(coordinatorSocket.getOutputStream());
				logger.info("opened output stream for coordinator");
			} catch(IOException ioe){
				try{
					coordinatorInput.close();
					coordinatorSocket.close();
				} catch(Exception e){}
				logger.severe("failed to open output stream for coordinator");
				return;
			}
			
			try {
				JSONObject out = (JSONObject) (new JSONParser().parse(toSend));
				out.put("proxy_port", clientPort);
				coordinatorOutput.writeBytes(out.toString()+"\n");
				logger.info("sent message "+out+" to coordinator");
			} catch (Exception e) {
				try{
					coordinatorOutput.close();
					coordinatorInput.close();
					coordinatorSocket.close();
				} catch(Exception ioe){}
				logger.severe("failed to send message to coordinator");
				return;
			}
			
			try{
				logger.info("received "+coordinatorInput.readLine()+" from coordinator, closing connection");
				coordinatorOutput.close();
				coordinatorInput.close();
				coordinatorSocket.close();
			} catch(Exception ioe){}			
		}
		
		@Override
		public void run() {
			ServerSocket listenSocket = null;
			
			try {
				listenSocket = new ServerSocket(providerPort);
				logger.info("created new listen socket for provider");
			} catch (Exception e) {
				logger.severe("failed to creat listen socket for provider");
				return;
			}
			while(!Thread.currentThread().isInterrupted()){
				Socket providerSocket = null;
				BufferedReader providerInput = null;
				
				try{
					providerSocket = listenSocket.accept();
					logger.info("accepted new provider");
				}catch(Exception e){
					logger.info("failed to accept new provider");
					continue;
				}
				
				try{
					providerInput = new BufferedReader(new InputStreamReader(providerSocket.getInputStream()));
					logger.info("opened input stream for provider");
				} catch(IOException ioe){
					try{
						providerSocket.close();
					} catch(Exception e){}
					logger.severe("failed to open input stream for provider");
					continue;
				}
				
				try{
					providerOutput = new DataOutputStream(providerSocket.getOutputStream());
					logger.info("opened output stream for provider");
				} catch(IOException ioe){
					try{
						providerSocket.close();
					} catch(Exception e){}
					logger.severe("failed to open output stream for provider");
					continue;
				}

				String old_update = "";
				try{
					String update;
					boolean hasSend = false;
					while((update = providerInput.readLine()) != null){
						if (hasSend){
							sendToCoordinator(old_update, MessageType.CONCEAL);
							logger.info("send conceal for old objects from provider to coordinator");
						}
						sendToCoordinator(update, MessageType.PUBLISH);
						old_update = update;
						hasSend = true;
						logger.info("forward publish for new objects from provider to coordinator");
					}
				}catch(Exception e){
					try {
						sendToCoordinator(old_update, MessageType.CONCEAL);
						providerInput.close();
						providerOutput.close();
						providerSocket.close();
					} catch (IOException ioe) {}
					logger.severe("failed to forward from provider to coordinator, conceal old objects and close connection");
					continue;
				}
			}
			
			try {
				listenSocket.close();
			} catch (IOException e) {}
			logger.severe("publisher listen socket closed");			
		}		
	}
}

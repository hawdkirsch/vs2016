package vs.middleware;
/**
 * @author tichy
 * 
 * Coordinator class which coordinates proxy connection to the clients for access distributed different cranes
 */

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import vs.middleware.Proxy.MessageType;

public class Coordinator{
	Map<Integer, JSONObject> proxyMap;
	Logger logger;
	
	public static void main(String[] args) {
		new Coordinator();
	}
	
	public Coordinator(){
		logger = Logger.getLogger(Coordinator.class.getName());
		try {
			FileHandler fh = new FileHandler("coordinator.log");
			logger.addHandler(fh);
			fh.setFormatter(new SimpleFormatter());
			logger.info("startet without problem");
		} catch (Exception e) {
			logger.severe("log file could not be opened");
			return;
		}
		
		
		Properties prop = new Properties();
		InputStream in = getClass().getResourceAsStream("config.properties");
		try {
			prop.load(in);
			in.close();
		} catch(IOException e) {
			logger.severe("failed to load properties");
			return;
		}
		
		CoordinatorHandling c1 = new CoordinatorHandling(Integer.parseInt(prop.getProperty("COORDINATOR_PUBLISH_PORT")));
		CoordinatorHandling c2 = new CoordinatorHandling(Integer.parseInt(prop.getProperty("COORDINATOR_LOOKUP_PORT")));
		CoordinatorHandling c3 = new CoordinatorHandling(Integer.parseInt(prop.getProperty("COORDINATOR_CONCEAL_PORT")));
		
		(new Thread(c1)).start();
		(new Thread(c2)).start();
		(new Thread(c3)).start();
	}
	
	private class CoordinatorHandling implements Runnable{
		ServerSocket listenSocket = null;
		int port;

		public CoordinatorHandling(int port){			
			try {
				listenSocket = new ServerSocket(port);
			} catch (Exception e) {
				logger.severe("listen on port "+port+" failed");
				return;
			}
			
			this.port = port;
			proxyMap = new HashMap<>();
			logger.info("listening on port "+port);
		}
		
		public synchronized String handleMessage(JSONObject msg, int port){
			JSONObject reply = new JSONObject();
			
			switch(port){
				case Constants.COORDINATOR_LOOKUP_PORT:{
					Collection<JSONObject> proxys = proxyMap.values();
					
					int robot_count = 0;
					for(JSONObject proxy : proxys){
						reply.put("robot_"+(int) ((long) proxy.get("robot_id")), proxy.toString());
						robot_count++;
					}
					
					reply.put("robot_count", robot_count);
					logger.info("received lookup on port "+port);					
					break;
				}
				case Constants.COORDINATOR_PUBLISH_PORT:{
					if(!msg.containsKey("robot_id") || !msg.containsKey("proxy_ip") || !msg.containsKey("proxy_port")){
						reply.put("Status", "ERROR");
					} else {
						proxyMap.put((int) ((long) msg.get("robot_id")), msg);
						reply.put("Status", "OK");
					}
					logger.info("received publish message "+msg+" on port "+port);	
					break;
				}
				case Constants.COORDINATOR_CONCEAL_PORT:{
					if(!msg.containsKey("robot_id") || !msg.containsKey("proxy_ip") || !msg.containsKey("proxy_port")){
						reply.put("Status", "ERROR");
					} else {
						proxyMap.remove(Integer.parseInt((String) msg.get("robot_id")));
						reply.put("Status", "OK");
					}
					logger.info("received conceal message "+msg+" on port "+port);
					break;
				}
				default:{
					reply.put("Status", "ERROR");
					logger.info("received message on unknown port "+port);
					break;
				}
			}

			logger.info("replying "+reply.toString());
			return reply.toString();
		}
		
		@Override
		public void run() {
			Socket clientSocket;
			while(!Thread.currentThread().isInterrupted()){
				BufferedReader clientInput = null;
				DataOutputStream clientOutput = null;
				
				try {
					clientSocket = listenSocket.accept();
					logger.info("accepting new client");
				} catch (Exception e) {
					logger.warning("accepting new client failed");
					continue;
				}
				
				//Open input stream for reader
				try{
					clientInput = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
					logger.info("opened input stream for client");
				} catch(IOException ioe){
					try{
						clientSocket.close();
					} catch(Exception e){}
					logger.warning("failed to open input stream for client");
					continue;
				}
				
				//Open output stream for writer
				try{
					clientOutput = new DataOutputStream(clientSocket.getOutputStream());
					logger.info("opened output stream for client");
				} catch(IOException ioe){
					try{
						clientInput.close();
						clientSocket.close();
					} catch(Exception e){}
					logger.warning("failed to open output stream for client");
					continue;
				}
				
				String textIn;
				try{
					while ((textIn = clientInput.readLine()) == null){};
					logger.info("received message from client");
				} catch (Exception ioe){
					try{
						clientOutput.close();
						clientSocket.close();
						clientInput.close();
					} catch(Exception e){}
					logger.warning("failed to receive message from client");
					continue;
				}
				
				try{
					clientOutput.writeBytes(handleMessage((JSONObject) new JSONParser().parse(textIn), port) + "\n");
					logger.info("wrote answer to client");
				} catch (Exception ioe){
					try{
						clientInput.close();
						clientSocket.close();
						clientOutput.close();
					} catch(Exception e){}
					logger.warning("failed to write message to client");
					continue;
				}
				
				try{
					clientInput.close();
					clientOutput.close();
					clientSocket.close();
				} catch(Exception e){}
				logger.info("closed connection to client");
			}
			
			try {
				listenSocket.close();
				logger.severe("listening socket closed");
			} catch (IOException e) {}
		}		
	}
}

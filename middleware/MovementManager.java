package vs.middleware;

import java.util.LinkedList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.cads.ev3.middeware.CaDSBase;
import org.cads.ev3.middeware.CaDSEV3StudentImplementation;
import org.cads.ev3.middeware.ICaDSEV3FeedBackListener;
import org.cads.ev3.middeware.ICaDSEV3StatusListener;

public class MovementManager{
	private static MovementManager instance;	
	private static CaDSEV3StudentImplementation caller;	
	
	/**
	 * Shared objects with the MovementCallers, to pass the movements.
	 * The queues contain the buffered movements and the mutex' lock the access to the queues.
	 * This is necessary because the queues will be dequeued in different, concurrent threads. 
	 */
	private static LinkedList<Movement> horizontalFifo;
	private static LinkedList<Movement> verticalFifo;
	
	private static Lock horizontalMutex = new ReentrantLock(true);
	private static Lock verticalMutex = new ReentrantLock(true);
	private final Logger movementManagerLog = Logger.getLogger(Consumer.class.getName());
	
	enum Movement {
		LEFT,
		RIGHT,
		UP,
		DOWN
	}
	
	private MovementManager(ICaDSEV3StatusListener sls, ICaDSEV3FeedBackListener fls){
		horizontalFifo = new LinkedList<Movement>();
		verticalFifo = new LinkedList<Movement>();		
		
		caller = new CaDSEV3StudentImplementation(new CaDSBase(), sls, fls);
		
		MovementCaller horizontalThread = new MovementCaller(caller, horizontalFifo, horizontalMutex);
		MovementCaller verticalThread = new MovementCaller(caller, verticalFifo, verticalMutex);
		
		(new Thread(horizontalThread)).start();
		movementManagerLog.log(Level.FINE, "[INFO] horizontal movement thread started");
		(new Thread(verticalThread)).start();
		movementManagerLog.log(Level.FINE, "[INFO] vertical movement thread started");
	}
	
	/**
	 * Singleton Pattern
	 * This class is allowed to only be instanced once. If it doesn't exist yet it will be created.
	 * @return The instance.
	 */
	public static MovementManager getInstance(Listener sls, ICaDSEV3FeedBackListener fls){
		if (instance == null){
			instance = new MovementManager(sls, fls);
		}
		
		return instance;
	}	

	/**
	 * Add the move up movement in the vertical movement queue.
	 */
	public void moveUp() {
		verticalMutex.lock();
		verticalFifo.add(Movement.UP);
		verticalMutex.unlock();
	}

	/**
	 * Add the move down movement in the vertical movement queue.
	 */
	public void moveDown() {
		verticalMutex.lock();
		verticalFifo.add(Movement.DOWN);
		verticalMutex.unlock();
	}

	/**
	 * Add the move left movement in the horizontal movement queue.
	 */
	public void moveLeft() {
		horizontalMutex.lock();
		horizontalFifo.add(Movement.LEFT);
		horizontalMutex.unlock();		
	}

	/**
	 * Add the move right movement in the horizontal movement queue.
	 */
	public void moveRight() {
		horizontalMutex.lock();
		horizontalFifo.add(Movement.RIGHT);
		horizontalMutex.unlock();
	}
	
	/**
	 * Stop the horizontal movement.
	 */
	public void stopH(){
		horizontalMutex.lock();
		caller.stop_h();
		horizontalMutex.unlock();
	}
	
	/**
	 * Stop the vertical movement.
	 */
	public void stopV(){
		verticalMutex.lock();
		caller.stop_v();
		verticalMutex.unlock();
	}
	
	/**
	 * Do the grab movement.
	 */
	public void doGrab(){
		caller.doClose();
	}
	
	/**
	 * Do the release movement.
	 */
	public void doRelease(){
		caller.doOpen();
	}
}

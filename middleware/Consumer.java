package vs.middleware;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.cads.ev3.gui.ICaDSGUIUpdater;
import org.cads.ev3.rmi.ICaDSRMIConsumer;
import org.cads.ev3.swing.CaDSGripperGUISwing;
import org.json.simple.JSONObject;

import cadSRMIInterface.IIDLCaDSEV3RMIMoveGrapper;
import cadSRMIInterface.IIDLCaDSEV3RMIMoveHorizontal;
import cadSRMIInterface.IIDLCaDSEV3RMIMoveVertical;
import vs.middleware.generated.*;

/**
 * 
 * @author tichy
 * 
 * Consumer class converts gui input into json strings and send them to the server on a predefined port,
 * as well is listing for an acknowledgment of the server to update the GUI in case of an error on the hardware side
 *
 */
public class Consumer implements IIDLCaDSEV3RMIMoveGrapper, IIDLCaDSEV3RMIMoveHorizontal, IIDLCaDSEV3RMIMoveVertical,
ICaDSRMIConsumer{
	private static Logger consumerLog;
	private Properties prop;
	private IGrabberMotor grabber;
	private IHorizontalMotor horizontal;
	private IVerticalMotor vertical;
	private int currentRobot;
	
	private int verticalPercent;
	private int horizontalPercent;
	
	/**
	 * Starts the client
	 * @param args '-r' will start the hardware, no argument the simulation
	 */
	public static void main (String[] args) {
			Consumer c = new Consumer();
			try {
				new CaDSGripperGUISwing(c, c, c, c);
			} catch (Exception e) {
				e.printStackTrace();
			}
	}
	
	public Consumer(){
		try{
			consumerLog = Logger.getLogger(Consumer.class.getName());
			
			prop = new Properties();
			InputStream in = getClass().getResourceAsStream("config.properties");
			prop.load(in);
			in.close();
						
			grabber = new GrabberMotorImpl();
			horizontal = new HorizontalMotorImpl();
			vertical = new VerticalMotorImpl();	
			
			currentRobot = 0;
			verticalPercent = 50;
			horizontalPercent = 50;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * register a new service in the GUI
	 */
	@Override
	public void register(ICaDSGUIUpdater arg0) {
		int robotCount = CommunicationClient.getInstance().lookup();

		if(robotCount==0){
			//error message
			return;
		}
		
		for(int i=1; i<=robotCount; i++){
			arg0.addService("Roboter "+i);
		}
				
		arg0.setChoosenService("Roboter 1", -1, -1);
		currentRobot = 1;
	}

	/**
	 * On GUI dropbox update
	 */
	@Override
	public void update(String arg0) {
		currentRobot = Integer.parseInt(arg0.replaceAll("\\D+", "")); //remove all non-digits and parse to integer (Roboter 1 => 1)
		vertical.moveVerticalToDegree(currentRobot, verticalPercent);
		horizontal.moveHorizontalToDegree(currentRobot, horizontalPercent);
	}
	
	/**
	 * GUI sends client to move the crane vertically
	 */
	@Override
	public void moveVerticalToPercent(int arg0, int arg1) throws Exception {
		consumerLog.log(Level.FINE, "[CLIENT] GRABBER -> VERTICAL ["+ arg1 + "%]"); //level.fine is used for debugging logs
		vertical.moveVerticalToDegree(currentRobot, arg1);
		verticalPercent = arg1;
	}

	/**
	 * GUI sends client to move the crane horizontally
	 */
	@Override
	public void moveMoveHorizontalToPercent(int arg0, int arg1) throws Exception {
		consumerLog.log(Level.FINE, "[CLIENT] GRABBER -> Horizontal ["+ arg1 + "%]");
		horizontal.moveHorizontalToDegree(currentRobot, arg1);
		horizontalPercent = arg1;
	}

	/**
	 * not implemented yet
	 */
	@Override
	public void stop(int arg0) throws Exception {
//		System.out.println("Stop movement....");
	}

	/**
	 * GUI sends client to open the grabber
	 */
	@Override
	public void openIT(int arg0) throws Exception {
		consumerLog.log(Level.FINE, "[CLIENT] GRABBER -> OPEN");
		grabber.doGrab(currentRobot, Integer.parseInt((prop.getProperty("GRABBER_OPEN"))));
	}

	/**
	 * GUI sends client to close the Grabber
	 */
	@Override
	public void closeIT(int arg0) throws Exception {
		consumerLog.log(Level.FINE, "[CLIENT] GRABBER -> CLOSE");
		grabber.doGrab(currentRobot, Integer.parseInt((prop.getProperty("GRABBER_CLOSE"))));
	}
}
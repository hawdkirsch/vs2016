package vs.middleware;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class CommunicationClient {
	private Logger logger;
	private Properties properties;
	private HashMap<Integer, DataOutputStream> proxyStreamMap;
	private static CommunicationClient instance;
	
	private CommunicationClient(){
		logger = Logger.getLogger(Consumer.class.getName());
		try {
			FileHandler fh = new FileHandler("communication_client.log");
			logger.addHandler(fh);
			fh.setFormatter(new SimpleFormatter());
			logger.info("startet without problem");
		} catch (Exception e) {
			logger.severe("log file could not be opened");
			return;
		}
		
		properties = new Properties();
		InputStream in = getClass().getResourceAsStream("config.properties");
		try {
			properties.load(in);
			in.close();
		} catch(IOException e) {
			logger.severe("failed to open properties");
			return;
		}	
		
		proxyStreamMap = new HashMap<>();
	}
	
	public static CommunicationClient getInstance(){
		if (instance==null){
			instance = new CommunicationClient();
		}
		
		return instance;
	}
	
	public int lookup(){
		String address = properties.getProperty("COORDINATOR_IP");
		int port = Integer.parseInt(properties.getProperty("COORDINATOR_LOOKUP_PORT"));
		Socket coordinatorSocket;
		BufferedReader coordinatorInput;
		DataOutputStream coordinatorOutput;
		
		try{
			coordinatorSocket = new Socket(address, port);
			logger.info("connected to coordinator. address "+address+", port "+port);
		} catch(IOException e){
			logger.severe("failed to connect to coordinator. address "+address+", port "+port);
			return 0;
		}
		
		try{
			coordinatorInput = new BufferedReader(new InputStreamReader(coordinatorSocket.getInputStream()));
			logger.info("opened input stream for coordinator");
		} catch(IOException ioe){
			try{
				coordinatorSocket.close();
			} catch(Exception e){}
			logger.severe("failed to open input stream for coordinator");
			return 0;
		}
		
		try{
			coordinatorOutput = new DataOutputStream(coordinatorSocket.getOutputStream());
			logger.info("opened output stream for coordinator");
		} catch(IOException ioe){
			try{
				coordinatorInput.close();
				coordinatorSocket.close();
			} catch(Exception e){}
			logger.severe("failed to open output stream for coordinator");
			return 0;
		}
		
		String textIn;
		try {
			JSONObject request = new JSONObject();
			request.put("LookUp", "LookUp");
			coordinatorOutput.writeBytes(request.toString() + "\n");
			logger.info("sent lookup message to coordinator");
		} catch (IOException ioe) {
			try{
				coordinatorInput.close();
				coordinatorOutput.close();
				coordinatorSocket.close();
			} catch(Exception e){}
			logger.severe("failed to send lookup message to coordinator");
			return 0;
		}
		
		JSONObject jsonIn = null;
		try {
			textIn = coordinatorInput.readLine();
			jsonIn = (JSONObject) (new JSONParser().parse(textIn));
			logger.info("received lookup reply from coordinator "+textIn);
		} catch (Exception pe) {
			try{
				coordinatorInput.close();
				coordinatorOutput.close();
				coordinatorSocket.close();
			} catch(Exception e){}
			logger.severe("failed to receive lookup reply from coordinator");
			return 0;
		}
		
		
		try {
			coordinatorInput.close();
			coordinatorOutput.close();
			coordinatorSocket.close();
		} catch (IOException e) {			
		}
		logger.info("closed connection to coordinator and process message");
		
		
		return processCoordinatorMessage(jsonIn);
	}
	
	public void sendRemoteMethodCall(JSONObject call){
		if (call == null || !call.containsKey("robot_id")){
			logger.severe("remote method call didn't include robot ID");
			return;
		}
		
		int robotID = (int) call.get("robot_id");
		
		if (!proxyStreamMap.containsKey(robotID) || !call.containsKey("degree") || !call.containsKey("object") || !call.containsKey("method")){
			logger.severe("parameter in call are incorrect "+call.toString());
			return;
		}
		
		DataOutputStream proxyOutput = proxyStreamMap.get(robotID);	

		try {
			proxyOutput.writeBytes(call.toString() + "\n");
			logger.info("sent call to proxy "+call.toString());
		} catch (IOException e) {
			logger.severe("failed to send call to proxy "+call.toString());
		} 
		
		return;
	}
	
	private int processCoordinatorMessage(JSONObject jsonIn) {
		int robotCount;
		
		if (jsonIn == null || !jsonIn.containsKey("robot_count")){
			logger.severe("coordinator message didn't include robot count "+jsonIn.toString());
			return 0;
		}
		
		robotCount = (int) ((long) jsonIn.get("robot_count"));
		int ret = robotCount;

		logger.info("received coordinator message "+jsonIn.toString());
		
		int i = 0;
		while(robotCount > 0){
			i++;
			if (!jsonIn.containsKey("robot_"+i)){
				continue;
			}
			
			String jText = (String) jsonIn.get("robot_"+i);
			JSONObject proxy = null;
			robotCount = robotCount-1;
			
			try {
				proxy = (JSONObject) (new JSONParser().parse(jText));
				logger.info("robot_"+i+" identified in lookup");
			} catch (ParseException e) {
				logger.severe("robot_"+i+" info couldn't be parsed "+jText);
				continue;
			}
			
			if (proxy == null || !proxy.containsKey("proxy_ip") || !proxy.containsKey("proxy_port")){
				logger.severe("robot_"+i+" info didn't include proxy info "+jText);
				continue;
			}
			
			Socket proxySocket = null;
			try {
				proxySocket = new Socket((String) proxy.get("proxy_ip"), (int) ((long) proxy.get("proxy_port")));
				logger.info("robot_"+i+" proxy socket opened "+jText);
			} catch (Exception e) {
				logger.severe("robot_"+i+" failed to open proxy socket "+jText);
				continue;
			}
			
			DataOutputStream proxyOutput = null;
			try{
				proxyOutput = new DataOutputStream(proxySocket.getOutputStream());
				logger.info("robot_"+i+" proxy output stream opened");
			} catch(IOException ioe){
				try {
					proxySocket.close();
				} catch (IOException e) {}
				logger.severe("robot_"+i+" failed to open proxy output stream");
				continue;
			}
			
			proxyStreamMap.put(i, proxyOutput);
			logger.info("added robot_"+i+" to proxy map");
		}
		
		return ret;
	}
}



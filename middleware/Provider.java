package vs.middleware;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.cads.ev3.middeware.CaDSBase;
import vs.middleware.generated.*;

public class Provider implements IGrabberMotor, IHorizontalMotor, IVerticalMotor{	
	private static Listener server;
	private Properties prop = new Properties();
	
	
	public static void main(String... args){
		boolean real = false;
		String address = "";
		int port = 0;
		int count = 0;
		int which = 0;
		
		for(String s : args){
			switch(s){
				case "-r":{
					real = true;
					break;
				}
				case "-p":{
					port = Integer.parseInt(args[count+1]);
					break;
				}
				case "-i":{
					address = args[count+1];
					break;
				}
				case "-w":{
					which = Integer.parseInt(args[count+1]);
					break;
				}
			}
			count++;
		}
		
		server = Listener.getInstance();
		CommunicationServer cs = CommunicationServer.getInstance(address, port, which);
		(new Thread(cs)).start();
		(new Thread(new Skeleton())).start();
		cs.sendPublish();
		
		/**
		 * Since we have a simulation and (obviously) the real system, this is to easily switch between them
		 * through the command line.
		 */
		
		
		
		
		try {
			if (real){			
				CaDSBase.start(CaDSBase.REAL, server, server);			
			} else {
				CaDSBase.start(CaDSBase.SIMULATION, server, server);
			}
		} catch (Exception e) {}
	}
	
	public Provider(){
		prop = new Properties();
		InputStream in = getClass().getResourceAsStream("config.properties");
		try {
			prop.load(in);
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}



	@Override
	public void moveVerticalToDegree(int arg1, int arg2) {
		server.setPosition(Integer.parseInt((prop.getProperty("VERTICAL"))), arg2);
	}



	@Override
	public void moveHorizontalToDegree(int arg1, int arg2) {
		server.setPosition(Integer.parseInt((prop.getProperty("HORIZONTAL"))), arg2);
	}



	@Override
	public void doGrab(int arg1, int arg2) {
		server.setPosition(Integer.parseInt((prop.getProperty("GRABBER"))), arg2);
	}
}
